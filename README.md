# Scrollspy #

This is DEMO for scrollspy plugin using vue.js framework. This DEMO is not using just pure vue.js but vue-loader https://vue-loader.vuejs.org/en/. vue-loader is a loader for webpack that can transform Vue components written in the following format into a plain JavaScript module.

Thanks to vue-loader we can create *.vue files which can include <template>, <script> and <styles> parts which are automatically processed (for example styles with proper param can automatically compile its scss content into pure css and the same can be done with script part so we can use for example coffeescrypt inside).

Main file is App.vue file and its directive placed in src/directive/scrollspy which is including main directive with core algoritm for scrollspy functionality. This directive is included in App.vue js and initiated by v-scrollspy param within main div #app.

There is hot reload functionality so you can change any scss, html or js code and it will be updated on background in your browser without any page refresh. Cool stuff. There can bee even JS linter added. It is not part of this DEMO.

### What is this repository for? ###

* Just a quick test of vue.js
* 1.0

### How do I get set up? ###

* npm install -g vue-cli
* npm install
* npm run dev # ready to go!
