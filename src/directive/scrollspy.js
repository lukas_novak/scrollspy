export default {

  inserted: function (el) {
    'use strict';

    var section = document.querySelectorAll(".section");
    var sections = {};
    var i = 0;
    var mobileMenu = document.getElementById("mobile-menu");

    mobileMenu.addEventListener("change", mobileMenuHook);

    function mobileMenuHook(el) {
        window.location.href = "#" + el.target.options[el.target.selectedIndex].value;
    }

    Array.prototype.forEach.call(section, function(e) {
      sections[e.id] = e.offsetTop;
    });

    window.onscroll = function() {
      var scrollPosition = document.documentElement.scrollTop || document.body.scrollTop;

      for (i in sections) {
        if (sections[i] <= scrollPosition) {
          document.querySelector('.active').setAttribute('class', ' ');
          document.querySelector('a[href*=' + i + ']').setAttribute('class', 'active');
        }
      }
    };
  }
}
